package builder.inter;

import builder.Computer;

import java.time.LocalDate;

public interface ComputerBuilder {

    // klik ctrl klik+przytyrzymaj ctra + kursor w dół !!!!!!!!!!!

    Computer build();
        ComputerBuilder ghz(double ghz);
        ComputerBuilder ram(int ram);
        ComputerBuilder screenInches (double screenInches);
        ComputerBuilder productionDate (LocalDate productionDate);
        ComputerBuilder weight(double weight);
        ComputerBuilder os(String os);

        // zawsze zwracamy this - ComputerBuilder wtedy możemy korzystac fluent builder
        // .ghz
        // .ram etc




}
