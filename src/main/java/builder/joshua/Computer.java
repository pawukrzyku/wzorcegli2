package builder.joshua;

import java.time.LocalDate;

public class Computer {

    private double ghz;
    private int ram;
    private double screenInches;
    private LocalDate productionDate;
    private double weight;
    private String os;

    private Computer(Builder builder) {
        this.ghz = builder.ghz;
        this.ram = builder.ram;
        this.screenInches = builder.screenInches;
        this.productionDate = builder.productionDate;
        this.weight = builder.weight;
        this.os = builder.os;
    }

    public double getGhz() {
        return ghz;
    }

    public void setGhz(double ghz) {
        this.ghz = ghz;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public double getScreenInches() {
        return screenInches;
    }

    public void setScreenInches(double screenInches) {
        this.screenInches = screenInches;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public static class Builder {
        private double ghz;
        private int ram;
        private double screenInches;
        private LocalDate productionDate;
        private double weight;
        private String os;

        public void build() {
            return;
        }

        public Builder ghz(double ghz) {
            this.ghz = ghz;
            return this;
        }

        public Builder ram(int ram) {
            this.ram = ram;
            return this;
        }

        public Builder screenInches(double screenInches) {
            this.screenInches = screenInches;
            return this;
        }

        public Builder productionDate(LocalDate productionDate) {
            this.productionDate = productionDate;
            return this;
        }

        public Builder weight(double weight) {
            this.weight = weight;
            return this;
        }

        public Builder os(String os) {
            this.os = os;
            return this;
        }
    }
}
