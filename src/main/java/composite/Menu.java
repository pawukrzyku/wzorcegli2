package composite;

import java.util.ArrayList;
import java.util.List;

//Klasa Node służy do przechowania całego Menu np Menu sniadaniowe ktore bedzie miało kolejne MenuItemy


public class Menu extends MenuComponent {

    private String name;
    private String description;
    List<MenuComponent> menuComponents = new ArrayList<>();

    @Override
    public void add(MenuComponent component) {
        menuComponents.add(component);
    }

    @Override
    public void remove(MenuComponent component) {
        menuComponents.remove(component);
    }

    public Menu(String name, String description) {
        this.name = name;
        this.description = description;

    }

    @Override
    public String print() {
        return "Menu{" +
                name + "\n" +
                description + "\n" +
                "----------------------------";
    }
}
