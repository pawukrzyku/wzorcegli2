package composite;

public class Main {

    public static void main(String[] args) {

        MenuComponent breakfastMen = new Menu("Menu śniadaniowe", "MS");
        MenuComponent dinnerMenu = new Menu("Obiadowe", "lunch");
        MenuComponent alcoholMenu = new Menu("alkohole %", "%%%");
        MenuComponent allMenus = new Menu("menu", "allmenu");

        allMenus.add(breakfastMen);
        allMenus.add(dinnerMenu);
        allMenus.add(alcoholMenu);

        MenuComponent beer = new MenuItem(4.0, "Tatra", "piwo małe", false);

        alcoholMenu.add(beer);

        Waitress waitress = new Waitress(allMenus);

        waitress.print();



    }

}
