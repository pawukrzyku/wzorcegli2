package composite;

public class Waitress {

    private MenuComponent allMenus;

    public Waitress(MenuComponent allMenus){
        this.allMenus = allMenus;
    }

    public void print(){
        System.out.println(allMenus.print());
    }
}
