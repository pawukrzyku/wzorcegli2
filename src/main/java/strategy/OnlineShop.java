package strategy;

public class OnlineShop {

    private PaymentStrategy paymentStrategy;

    public OnlineShop(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public void paymentMethod() {
        System.out.println("Wybrałeś metodę płatności: ");
        paymentStrategy.payMethod();
    }

    public OnlineShop setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
        return this;
    }
}
