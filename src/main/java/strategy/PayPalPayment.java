package strategy;

public class PayPalPayment implements PaymentStrategy {


    @Override
    public void payMethod() {
        System.out.println("Płatność PayPal");
    }
}
