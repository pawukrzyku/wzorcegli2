package strategy;

public class TransferPayment implements PaymentStrategy {
    @Override
    public void payMethod() {
        System.out.println("Płatność przelewem");
    }
}
