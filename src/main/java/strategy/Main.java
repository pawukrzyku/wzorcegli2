package strategy;

//    Zadanie 3:
//
//    klasa OnlineShop
//- można płacić za zakupy różnymi sposobami
//- sposób dokonywania zakupów jest niezależny od sposobu płatności
//- płacić można: kartą, przelewem, PayPal
//
//    Zadanie: Strategy dla różnych metod płatności

public class Main {
    public static void main(String[] args) {

        PaymentStrategy cashStrategy = new CashStrategy();

        OnlineShop shop = new OnlineShop(cashStrategy);

        shop.paymentMethod();

        shop.setPaymentStrategy(new CardPayment());
        shop.paymentMethod();

    }
}

// stworz klase order