package strategy;

public class CardPayment implements PaymentStrategy {
    @Override
    public void payMethod() {
        System.out.println("Płatność kartą");
    }
}
